/**
 * @author timger <yishenggudou@gmail.com>
 * @timger http://weibo.com/zhanghaibo
 * Copyright (c) 2008-2011 timger - released under MIT License {{{
 */
data={};
zh={'skills':[
        {'name':'linux','logo':'images/linux-logo.jpg','Proficiency':'熟练','star':'****','age':'1.5年','text':'熟练使用linux各种日常操作，服务器的维护，网站的部署，apache，nginx的部署，部署和维护过多个网站,有使用过AWS GAE 等一些云计算产品'},
        {'name':'python','logo':'images/python_card.png','Proficiency':'熟练','star':'****','age':'~3年','text':'熟悉各种语法和三方库。gui，web都有过尝试，有多个django框架实战经验，bottle等其他框架也有了解,在google appengine，aws环境上写过多个项目'},
        {'name':'database','logo':'images/database.png','Proficiency':'熟练','star':'****','age':'>2年','text':'熟练使用mysql,sql语法，有了解nosql技术，mongodb使用经验'},
        {'name':'javascript','logo':'','Proficiency':'熟练','star':'****','age':'>2年','text':'熟悉js语法,和dom对象,熟练使用jquery以及各种三方库实现各种效果，最近尝试html5+js编写web游戏,node.js什马的就没折腾了'},
        {'name':'html&css','logo':'images/html.png','Proficiency':'熟练','star':'****','age':'>3年','text':'熟练编写html页面 对css3 html5 和移动web 开发有一定了解'},
       {'name':'php','logo':'','Proficiency':'熟练','star':'****','text':'熟悉php基本语法，有过三个月的php实战经验，编写过php网站'},
       {'name':'shell','logo':'','Proficiency':'熟练','star':'****','text':'会编写简单常用的shell脚本,自动化，测试'},
       {'name':'工具类','logo':'','Proficiency':'熟练','star':'****','text':'熟练vim编辑器,svn，hg，git各种协作工具'},
       {'name':'学习能力','logo':'','Proficiency':'熟练','star':'****','text':'英语四级,顺利阅读各种英语技术文档，stackoverflow，quora会员'},
       {'name':'其他','logo':'','Proficiency':'熟练','star':'****','text':' ruby有看过一点入门，什么erlang Haskell Lisp的都没搞 ^_^'},
                ],
    'infos':[
    {'key':'Email','value':'yishenggudou@gmail.com'},
    {'key':'Weibo','value':'http://t.sina.com/zhanghaibo'},
    {'key':'Blog','value':'http://blog.timger.info'},
    {'key':'Web','value':'http://about.timger.info'},
    {'key':'QQ','value':'154693982'},
    ],
    'edus':[
    {'time':'2003-2006','school':'慈利一中','text':'这三年在加油高考，考过倒数也考过前排,最终语文高考语文不及格，英语刚及格，还好进湖大'},
    {'time':'2006-2010','school':'湖南大学','text':'这四年在湖大有意义地渡过,挂过科,也拿过奖，干过社团，干过班干，结识一帮好兄弟,'}
    ],
    'jobs':[
    {'time':'2010.10-now','company':'华夏心理','text':'python软件编写和网站系统维护,服务器配置,数据备份,负责网站数据抓取配合网站开发写自动化脚本.'},
    ],
    'projects':[
    {'name':'Github','url':'https://github.com/yishenggudou','text':'Github上的项目'},
    {'name':'bitbucket','url':'https://bitbucket.org/yishenggudou','text':'bitbucket上的项目'},
    {'name':'GoogleCode','url':'http://code.google.com/u/112027857348697487239/','text':'GoogleCode上的项目比较少'},
    {'name':'晚会工具','url':'http://code.google.com/p/celebration-assistant/','text':"wxpython写的一个小游戏,该项目是与buckler(腾讯)以及血染岳麓(华为)一起编写的，项目主要是做一个桌面软件，定位于用户在晚会或者其他聚会场合下的辅助工具，项目主要用到了wxpython和javascript以及html"},
    {'name':'octopus','url':"http://code.google.com/u/112027857348697487239/",'text':"<p>爬虫系统</p><p>支持多线程多进程</p><p>分段任务</p><p>web管理界面,cron脚本自动执行,日志处理</p><p>对于大规模大批量的网站抓取很有用[目前抓取网站数量在1450个]，抓取任务用数据库存储,可在web编辑界面里动态添加抓取任务，随时修改页面分析代码</p><p>此项目稳定运行3个多月,演示数据参见<a href='http://code.google.com/p/timger-database-backup/source/browse/trunk/serverxd_item_2011_10_14_copy.sql'>example</a></p><p>项目用到sqlalchemy,mongodb,线程池...</p>"},
    {'name':'101xd','url':"https://bitbucket.org/yishenggudou/101xd/overview",'text':"<p><a href='http://www.101xd.com/'>101xd</a>电子商务供货网站</p><p>php编写,其中js和ajax部分大部分为我编写</p><p>此网站的工作环境搭建（开发环境和线上环境）都是本人负责,php编写部分和另一位phper一起编写</p>"},
    {'name':'果味','url':'http://guowee.timger.info','text':"<p>此项目为帮别人代写，因其运营问题网站已经下线，此处挂一实例网站</p><p>项目由django编写,用到爬虫，分析页面，等技术</p>"},
    {'name':'hibox','url':'http://hibox.me','text':"<p>此为一产品展示网站，有下单功能</p><p>django编写全占包括js，html，css全部由我一人编写</p>"},
    {'name':'artdna','url':'http://artdna.me','text':"<p>此为一基于艺术界的名片展示系统</p><p>django编写，全占包括js，html，css全部由我一人编写</p><p>有使用mongodb数据库</p>"},
    {'name':'newbeelab','url':'http://newbeelab.com','text':"<p>团队展示网站</p><p>纯js+html，全部由我一人编写</p><p></p>"},
    {'name':'各种gae项目','url':'','text':"<p>多个GAE项目</p><p>一大堆，组织要求保密,不给网址了</p><p></p>"},
    ],
    'me':{'text':"<p><img src='http://commondatastorage.googleapis.com/haibo/temp/resume.jpg' title='文艺青年时候的我' width='480px'/></p><br/><p>湖南张家界人士，湖南大学化学专业理学学士.</p><br/><p><strong>电子产品迷</strong>：一直对于电子产品和计算机有很浓厚的兴趣，小时候经常修理各种家里面的电器，对各种新奇的电子产品有难以抑制的研究冲动,<br/>在家负责帮邻居修家电[dvd，电视，空调。等],在学校是专业装机户[负责男生本专业的装机装系统事业]</p><br/><p><strong>计算机迷</strong>：接触到计算机之后就一发不可收拾，基本上没有停止对计算机的研究，尤其喜欢上google之后，整天浸泡在各类IT技术的研究中。</p><br/><p><strong>Python迷</strong>：08年的时候接触Python,被python吸引了，一直学习python，熬过很多夜晚，在此过程中更加肯定对python对计算机喜爱，我觉得找到了像热爱生命一样热爱的兴趣。从一个大好文艺青年熬夜熬成了一个二逼程序猿..</p><br/><p><strong>努力成为Geek</strong>: 自认为自己是一个技术控，什么都想研究明白，很有geek精神，数理基础很不错（当年高考时语文不及格，英语刚及格进的湖大），关注各种新技术，快速学习以及满腹热情。<p><br/><p>blog:<a href='http://blog.timger.info'>blog.timger.info</a><p><p>Email:<a href='mailto:haibolib@gmail.com'>haibolib@gmail.com</a></p><p>QQ:#154693982</p><p>Phone:+8615611757008</p><p>douban:<a href='http://www.douban.com/people/yishenggudou/'>http://www.douban.com/people/yishenggudou/</a></p><p>微博：<a href='http://weibo.com/zhanghaibo'>@timger</a></p><p>Twitter：<a href='http://twitter.com/yishenggudou'>@yishenggudou</a></p>"}
    }


$(document).ready(function(){
        $('#skills').html($('#t_skills').tmpl(zh.skills));
        $('#edus').html($('#t_edus').tmpl(zh.edus));
        $('#projects').html($('#t_projects').tmpl(zh.projects));
        $('#jobs').html($('#t_jobs').tmpl(zh.jobs));
        $('#me').html($('#t_me').tmpl(zh.me));
        work();
        $('#hello').click()
        })
function work(){
    var li = ['edus','jobs','projects','me','skills'];
        $('a[tp]').click(function(){
                var _q = '#'+$(this).attr('tp');
                $('a[tp]').parent().removeClass('active');
                for (jj in li){
                var q_ ='#'+li[jj];
                $(q_).hide();
                }
                $(this).parent().addClass('active');
                $(_q).show();
                //return false;
                });
        li.forEach(function(elem){
            var q = '#'+elem;
            $(q).addClass('active');
            if (elem!='me'){
            $(q).hide();
            $(q).removeClass('active');
            }
            })
    }
function html5me(){
    var canvas = document.getElementById('mepic');
    canvas.style.width='480px';
    canvas.style.height='360px';
    var ctx = canvas.getContext('2d');
    this.wenyi = function(){
        ctx.font = "20pt Arial";
        ctx.fillText("文艺青年的我", 10, 50);
        img_wenyi = new Image();
        img_wenyi.src = 'http://commondatastorage.googleapis.com/haibo/temp/resume.jpg';
        ctx.drawImage(img_wenyi,10,80,360,240,200,200,10,10);
        }
    this.zhengchang = function(){
        ctx.font = "20pt Arial";
        ctx.fillText("正常的我", 10, 50);
    }
    this.erbi = function(){
        ctx.font = "20pt Arial";
        ctx.fillText("二逼青年的我", 10, 50);
    }
    }
